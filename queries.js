var fs = require('fs'),
  shell = require('shelljs'),
  audios = getFiles(process.argv[2]),
  Q = require('q'),
  PropertiesReader = require('properties-reader'),
  properties = PropertiesReader('properties'),
  access_key = properties.get('account.access_key'),
  access_secret = properties.get('account.access_secret'),
  millisec = require('millisec');
require('q-foreach')(Q);

// function per l'esecuzione di comandi shall
runproc = function (cmd, callback){
	var process = shell.exec(cmd, function(code, msg) {
    callback({code:code, msg: msg});
	});
	return process;
};

if (audios==false){
  console.log("\nTo run this program exec: 'node index.js <path queries folder>'\n");
} else {
  Q.forEach(audios,function(path){
    var deferred = Q.defer();
    var cmd1 = "python test.py "+path+" "+access_key+" "+access_secret;
    //console.log(cmd1);
    console.log("QUERY file: *** "+path+" ***");
    var mmss =
    runproc(cmd1,  function(out1){
      if(out1.code===0){
        console.log("OUTPUT:");
        var res = JSON.parse(out1.msg);
        console.log("message: "+res.status.msg);
        console.log("matches:");
        res.metadata.custom_files.forEach(function(file){
          console.log("\t title: "+file.title);
          console.log("\t audio_id: "+file.audio_id);
          console.log("\t acrid: "+file.acrid);
          console.log("\t play_offset: "+millisec(file.play_offset_ms).format('hh : mm : ss')+"\n");
        })
        console.log("\n");
        deferred.resolve("Tutto ok");
      }
    })

    return deferred.promise;
  })
}

function getFiles (dir, files_){
  try{
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
  }catch (ex){
    console.log("\nError: "+ex.message);
    return false;
  }
}

var fs = require('fs'),
  shell = require('shelljs'),
  audios = getFiles(process.argv[2]),
  PropertiesReader = require('properties-reader'),
  properties = PropertiesReader('properties'),
  access_key = properties.get('console_api_keys.access_key'),
  access_secret = properties.get('console_api_keys.access_secret');
  Q = require('q');
require('q-foreach')(Q);

// function per l'esecuzione di comandi shall
runproc = function (cmd, callback){
	var process = shell.exec(cmd, function(code, msg) {
    callback({code:code, msg: msg});
	});
	return process;
};
if (audios==false){
  console.log("\nTo run this program exec: 'node builddb.js <path audios folder>'\n");
} else {
  try{
    Q.forEach(audios,function(path){
      var deferred = Q.defer();
      //console.log(path);
      //if (path === "audios/test2.mp3"){
        var name = path.split("/")[1];
        var folder = path.split("/")[0];
        var name = name.split(".mp3")[0];
        //console.log(folder);
        //console.log(name);
        var fileWav = name+".wav";
        var cmd1 = "./ffmpeg -y -i "+path+" -ac 1 -ar 8000 tmp/"+fileWav;
        //console.log(cmd1);
        runproc (cmd1, function(out1){
          if(out1.code===0){
            //console.log(out1);
            //./acrcloud_extr_db_fp my_song_2.wav
            var cmd2 = "./acrcloud_extr_db_fp tmp/"+fileWav;
            console.log(cmd2);
            runproc (cmd2, function(out2){
              if(out2.code===0){
                //console.log(out2);
                var uuid= require("uuid");
                var data = {
                  "title": name,
                  "audio_id": uuid.v4(),
                  "bucket_name": "TestUpload",
                  "data_type": "fingerprint"
                }
                var cmd3 = "rm -rf tmp/"+fileWav+"| python upload_audios.py tmp/"+fileWav+".lo '"+JSON.stringify(data)+"' "+access_key+" "+access_secret;
                //console.log("\n\nCOMANDO: "+cmd3+"\n\n");
                runproc(cmd3,  function(out3){
                  if(out3.code===0){
                    //console.log(out3);
                    deferred.resolve("Tutto ok");
                  }
                })
              }
            })
          }
        })
      //}
      return deferred.promise;
    })
  }catch(ex){
    console.log(ex.message);
  }
}

function getFiles (dir, files_){
  try{
    files_ = files_ || [];
    try{
      var files = fs.readdirSync(dir);
      for (var i in files){
          var name = dir + '/' + files[i];
          if (fs.statSync(name).isDirectory()){
              getFiles(name, files_);
          } else {
              files_.push(name);
          }
      }
    } catch(ex){
      console.log(ex.message);
      return (ex.message);
    }
    return files_;
  } catch (ex){
    console.log("\nError: "+ex.message);
    return false;
  }
}

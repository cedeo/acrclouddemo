# acrcloudDemo #
Questo progetto utilizza le api fornite da https://www.acrcloud.com/ per realizzare programmaticamente una base di dati popolata con degli audio fingerprint e successivamente interrogarla.

### Dipendenze ###
1. node.js
2. npm

### Installazione ###
Da terminale eseguire i seguenti comandi:

```
#!bash
git clone https://andreabonifacino@bitbucket.org/cedeo/acrclouddemo.git
cd acrclouddemo
npm install
```
### Linee guida ###

1. creare un account di prova su https://console.acrcloud.com/signup
2. creare un nuovo "bucket" seguendo la procedura al link https://github.com/acrcloud-demo/content-uploading-quick-trial
3. creare un progetto di "Audio e Video Recognition" seguendo la procedura al link https://github.com/acrcloud-demo/audio-recognition-quick-trial
4. aprire il file "properties" e sostituire le "account access keys" con quelle generate durante la creazione dell'account personale e reperibili alla pagina  https://eu-console.acrcloud.com/service/avr
5. sostituire le "console api keys" con quelle reperibili alla pagina personale https://eu-console.acrcloud.com/service/avr
6. creare una nuova cartella, "audios", da popolare con i file audio con cui si vuole creare una base di dati di audio fingerprint
7. creare una nuova cartella, "queries", da popolare con i file audio con cui si vuole interrogare la base di dati di audio fingerprint

### Esecuzione ###

Per popolare la base di dati eseguire da terminale il seguente comando:
```
#!bash

node builddb.js audios
```

Per interrogare la base di dati eseguire da terminale il seguente comando:

```
#!bash

node queries.js queries
```